# Programa Hidro Guardianes

Documento Oficial - EN-DESARROLLO   https://docs.google.com/document/d/1oEK6xqRwZYPKg-TdXHF4TFv0o4_nv9KxeiBPrcXriek/edit?usp=sharing

## Actividades del proyecto:

Fase 1: Diagnóstico y Estado actual de los Ríos de Palmares
Fase 2: Reforestación y Limpiezas
Fase 3: Implementación de Proyectos Específicos (Barreras AntiPlastico y otros)

## Enlaces Importantes

>> Proceso Administración de Donaciones   https://docs.google.com/document/d/1lNUnr_4FlAhKoFWrF0JSbduCSIs8HY-8TpMMUSsoYOU/edit?usp=sharing
>> Archivo de Donaciones Aguas Viven   https://gitlab.com/fundacion-aguas-viven/av-bd-donaciones/-/blob/main/av-bd-donaciones.json
>> Archivo de Donaciones Fundación Estampida   https://gitlab.com/fundacion-estampida/fe-bd-donaciones/-/blob/main/fe-bd-donaciones.json
>> Tareas abiertas en Repositorio Publico en GitLab   https://gitlab.com/groups/fundacion-estampida/-/issues


++AGREGANDO PARA PROBAR
